FROM adoptopenjdk/openjdk11:alpine
COPY ./target/demo-application-0.0.1-SNAPSHOT.jar demo-application-0.0.1-SNAPSHOT.jar
ENTRYPOINT [ "java" , "demo-application-0.0.1-SNAPSHOT.jar" ]
